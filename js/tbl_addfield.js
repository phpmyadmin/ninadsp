/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 * @fileoverview    functions used on the Table Add Field page
 * @name            Table Add Field
 *
 * @requires    jQuery
 * @requires    jQueryUI
 * @requires    js/functions.js
 *
 */

$(document).ready(function() {

    /**
     * AJAX event handler for 'Add/Save Field'
     *
     */
    $("#add_field_form").delegate("input[name=submit_num_fields], input[name=do_save_data]", 'click', function(event) {
        event.preventDefault();

        /**
         *  @var    the_form    object referring to the create table form
         */
        var the_form = $("#add_field_form");

        PMA_ajaxShowMessage(PMA_messages['strProcessingRequest']);
        $(the_form).append('<input type="hidden" name="ajax_request" value="true" />');

        if($(this).attr('name') == 'submit_num_fields') {
            //User wants to add more fields to the table
            $.post($(the_form).attr('action'), $(the_form).serialize() + "&submit_num_fields=" + $(this).val(), function(data) {
                $("#add_field_form").html(data);
                PMA_ajaxShowMessage("");
            }) //end $.post()
        }
        else if($(this).attr('name') == 'do_save_data') {
            $.post($(the_form).attr('action'), $(the_form).serialize() + "&do_save_data=" + $(this).val(), function(data) {
                if(data.success == true) {
                    PMA_ajaxShowMessage(data.message);
                }
                else {
                    PMA_ajaxShowMessage(data.error);
                }
            })
        }
    });
});